use master
go

create database bandocu
go

use bandocu
go

---------User(Nguoi dung)---------------------//
create table Users
(
	IdUsers int identity(1,1) primary key,
	Account	varchar(15),
	Password Varchar(10),
	Name nvarchar(max),
	Address nvarchar(max),
	Phone nvarchar(20),
	Email nvarchar(200),
	DateBirth DateTime,
	Sex nvarchar(5),
	Avatar image,
	Point int,
	IDPost	int
)
go

---------TypeCategory(Loai danh muc)---------------------//
create table TypeCategory
(
	IDType	varchar(10) primary key,
	NameCategory nvarchar(50)
)
go

---------Category(Danh muc)---------------------//
create table Category
(
	IDCategory	int identity(1,1) primary key,
	IDType	varchar(10),
	NameCategory nvarchar(50)

	foreign key(IDType) references TypeCategory(IDType),
)
go

---------Delivery(Giao hang)---------------------//
create table Delivery
(	
	IDDelivery	int,
	NameDelivery nvarchar(50)
)

---------Post(Bai viet)---------------------//
create table Post
(
	IDPost int identity(1,1) primary key,
	IDUsers int,
	Title	nvarchar(max),
	IDType	varchar(10), 
	IDCategory	int,
	DemandSale	nvarchar(max),
	Address	nvarchar(max),
	Sort	nvarchar(12),
	Picture	image,
	Price 	money,
	Status nvarchar(max),
	Description	nvarchar(max)

	foreign key (IDType) references TypeCategory(IDType),
	foreign key (IDCategory) references Category(IDCategory),
	foreign key (IDUsers) references Users(IdUsers),
)
go

select * from  dbo.Category
select * from  dbo.Delivery
select * from  dbo.Post
select * from  dbo.TypeCategory
select * from  dbo.Users

